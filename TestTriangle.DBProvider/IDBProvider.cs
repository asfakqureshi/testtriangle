﻿using System;
using System.Collections.Generic;
using System.Text;
using TestTriangle.Model;

namespace TestTriangle.DBProvider
{
    public interface IDBProvider
    {
        int Insert<T>(T data) where T : BaseEntity;

        IEnumerable<T> Get<T>() where T : class;
    }
}
