﻿using DapperExtensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using TestTriangle.DBProvider.Interface;
using TestTriangle.Model;

namespace TestTriangle.DBProvider.Repository
{
    //SOLI-LSP
    //Generic Class
    public abstract class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        IDBProvider dBProvider;
        public BaseRepository(IDBProvider dBProvider) 
        {
            this.dBProvider = dBProvider;
        }

        public virtual int Insert(T data) 
        {
           return dBProvider.Insert(data);
        }

        public virtual IEnumerable<T> Get() 
        {
            return dBProvider.Get<T>();
        }
    }
}
