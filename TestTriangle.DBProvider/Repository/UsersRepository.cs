﻿using DapperExtensions;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using TestTriangle.DBProvider.Interface;
using TestTriangle.Model;

namespace TestTriangle.DBProvider.Repository
{
    //SOLID - Open Close Principle 
    // We have User repo interface which impl. IBaseRepository
    // Now we have free dome to add more interface method or change/add validation before operation with override base function
    public class UsersRepository :  BaseRepository<Users>, IUserRepository
    {
        public UsersRepository(IDBProvider dBProvider): base(dBProvider)
        {

        }

        //Override base and add validation
        public override int Insert(Users data)
        {
            if(string.IsNullOrEmpty(data.Name))
            {
                return -1;
            }
            return base.Insert(data);
        }

    }
}
