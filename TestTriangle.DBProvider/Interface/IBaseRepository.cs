﻿using System;
using System.Collections.Generic;
using System.Text;
using TestTriangle.Model;

namespace TestTriangle.DBProvider.Interface
{
    public interface IBaseRepository<T> where T : BaseEntity
    {

        IEnumerable<T> Get();

        int Insert(T data);
    }
}
