﻿using DapperExtensions;
using System.Collections.Generic;
using System.Data.SqlClient;
using TestTriangle.Model;

namespace TestTriangle.DBProvider
{
    /// <summary>
    /// Wrapper on SQL Priver 
    /// Single Entry point for SQL (SOLID)
    /// Single Place change if Dapper update or use other ORM or Fix any bug
    /// </summary>
    public class SQLProvider : IDBProvider
    {
        private SqlConnection conn;
        public SQLProvider(SqlConnection conn)
        {
            this.conn = conn;

        }

        //Generic Method
        //We may add more method
        public int Insert<T>(T data) where T : BaseEntity
        {
            conn.Open();
            conn.Insert<T>(data);
            conn.Close();
            return data.Id;
        }

        public IEnumerable<T> Get<T>() where T : class
        {   
            conn.Open();
            var data =  conn.GetList<T>();
            conn.Close();
            return data;
        }
    }
}
