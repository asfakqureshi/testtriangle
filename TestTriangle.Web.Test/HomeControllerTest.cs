﻿using System.Collections.Generic;
using TestTriangle.Model;
using TestTriangle.Web.Controllers;
using Xunit;

namespace TestTriangle.Web.Test
{
    public class HomeControllerTest : BaseTest
    {

        [Fact]
        public void Success_Test_Get_View()
        {
            var controller = new HomeController(mockHttpHelper.Object)
            {
                ControllerContext = mockContext.Object
            };

            //Act
            var result = controller.Index().Result;

            //Assert
            Assert.NotNull(result);
            var viewModel = controller.ViewData.Model as List<Users>;
            Assert.NotNull(viewModel);
            Assert.Equal(mockList, viewModel);

        }

        [Fact]
        public void Success_Test_Post_View()
        {
            var controller = new HomeController(mockHttpHelper.Object)
            {
                ControllerContext = mockContext.Object
            };

            //Act
            var result = controller.Index(user).Result;

            //Assert
            Assert.NotNull(result);
            var viewModel = controller.ViewData.Model as List<Users>;
            Assert.NotNull(viewModel);
            Assert.Equal(mockList, viewModel);

        }

        [Fact]
        public void Fail_Test_Get_View()
        {
            var controller = new HomeController(null)
            {
                ControllerContext = mockContext.Object
            };

            //Act
            var result = controller.Index().Result;
            var viewModel = controller.ViewData.Model as List<Users>;
            //Assert
            Assert.NotNull(result);
            Assert.Null(viewModel);
           
        }

        [Fact]
        public void Fail_Test_Post_View()
        {
            var controller = new HomeController(mockHttpHelper.Object)
            {
                ControllerContext = mockContext.Object
            };

            //Act
            var result = controller.Index(null).Result;

            //Assert
            Assert.Null(result);
            var viewModel = controller.ViewData.Model as List<Users>;
            Assert.Null(viewModel);
            Assert.NotEqual(mockList, viewModel);
        }

    }
}
