﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestTriangle.Common;
using TestTriangle.Model;
using TestTriangle.Web.Controllers;
using Xunit;

namespace TestTriangle.Web.Test
{
    public class HttpHelperTest : BaseTest
    {
        [Fact]
        public void Success_Test_HttpHelper_Get()
        {
            var userResult = mockHttpHelper.Object.GetRequestAsync<Users>(Constant.UserUrl).Result;
            Assert.True(mockList.Equals(userResult));
        }

        [Fact]
        public void Fail_Test_HttpHelper_Get()
        {
            var userResult = mockHttpHelper.Object.GetRequestAsync<Users>("").Result;
            Assert.False(mockList.Equals(userResult));
        }

        [Fact]
        public void Success_Test_HttpHelper_Post()
        {
            var postReuslt = mockHttpHelper.Object.PostDataAsync<Users, int>(Constant.UserUrl, user).Result;
            Assert.True(postReuslt == 1);
        }

        [Fact]
        public void Fail_Test_HttpHelper_Post()
        {
            var postReuslt = mockHttpHelper.Object.PostDataAsync<Users, int>("", user).Result;
            Assert.False(postReuslt == 1);
        }


    }
}
