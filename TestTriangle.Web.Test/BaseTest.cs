﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestTriangle.Common;
using TestTriangle.Model;

namespace TestTriangle.Web.Test
{
    public class BaseTest
    {
        public const string DUMMY = "dummy";
        public Mock<ControllerContext> mockContext;
        public Mock<IHttpHelper> mockHttpHelper;
        public Users user;
        public List<Users> mockList;

        static BaseTest()
        {
            Constant.UrlConst = new Dictionary<string, string>();
            Constant.UrlConst.Add("ApiBaseUrl", "http://localhost");
            Constant.UrlConst.Add("UserUrl", "User");
        }

        public BaseTest()
        {
            SetupMocks();
        }

        private void SetupMocks()
        {
            mockContext = new Mock<ControllerContext>();
            mockHttpHelper = new Mock<IHttpHelper>();
            user = new Users { Name = DUMMY, Email = DUMMY, Phone = DUMMY };
            mockList = new List<Users>() { user };
            
            mockHttpHelper.Setup(x => x.PostDataAsync<Users, int>(Constant.UserUrl, user)).Returns(Task.FromResult(1));
            mockHttpHelper.Setup(x => x.GetRequestAsync<Users>(Constant.UserUrl)).Returns(Task.FromResult(mockList));

        }

    }
}
