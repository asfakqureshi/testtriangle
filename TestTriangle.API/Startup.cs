﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.AzureAD.UI;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Data.SqlClient;
using TestTriangle.DBProvider.Interface;
using TestTriangle.DBProvider;
using TestTriangle.DBProvider.Repository;

namespace TestTriangle.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(AzureADDefaults.BearerAuthenticationScheme)
                .AddAzureADBearer(options => Configuration.Bind("AzureAd", options));

            RegisterRepository(services);
            
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        //DI for SQL DBProvider
        public void RegisterRepository(IServiceCollection services)
        {
      
            //Use In-build dotnet core Injection
            //I also worked with Autofac and use it if require
            services.AddTransient<IDBProvider>(reqested =>
            {
                //Change Provider as per requirement like use MySQL or MongoDB
                //May use diffrent connection string based on tenant id, comes from header
                //SOLID - Easily change provider with other DB, Single Entry point for DB
                //No Depandancy SQL, need to write 1 wrapper.
                var connStr = Configuration.GetConnectionString("SQLConnection");
                SqlConnection sqlConnection = new SqlConnection(connStr);
                return new SQLProvider(sqlConnection);
            });

            //Peform DI for each repository
            //Instead of this I can write Generic function with reflection which will register all repository type.
            //Other wise we need to register each repository when we add new table
            services.AddTransient<IUserRepository>( reqested => 
            {
                var dbProvider = reqested.GetRequiredService<IDBProvider>();
                return new UsersRepository(dbProvider);
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}
