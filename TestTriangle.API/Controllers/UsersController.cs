﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TestTriangle.DBProvider.Interface;
using TestTriangle.Model;

namespace TestTriangle.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        IUserRepository userRepository;

        //userRepository Injected using DI
        public UsersController(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<Users>> Get()
        {
            return new ActionResult<IEnumerable<Users>>(userRepository.Get());
        }

        //We can create Enum for Error Code
        // POST api/values
        [HttpPost]
        public ActionResult<int> Post([FromBody] Users user)
        {
           if(user==null)
           {
                return new ActionResult<int>(0);
           }
           return new ActionResult<int>(userRepository.Insert(user));
        }

        
    }
}
