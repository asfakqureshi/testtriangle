﻿using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TestTriangle.Common
{
    public interface IHttpHelper
    {
        Task<AuthenticationResult> Authenticate();

        Task<List<T>> GetRequestAsync<T>(string path);

        Task<TReturn> PostDataAsync<TParam, TReturn>(string path, TParam data);
    }
}