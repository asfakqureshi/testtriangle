﻿using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace TestTriangle.Common
{
    //SOLID - Single Responsiblity
    //Singleton object by DI
    public class HttpHelper : IHttpHelper
    {
        private static readonly AuthenticationContext authContext;
        private static readonly ClientCredential cred;

        static HttpHelper()
        {
            authContext = new AuthenticationContext(Constant.Authority);
            cred = new ClientCredential(Constant.AzureAd.ClientId, Constant.AzureAd.ClientSecret);
        }

        //We can set token in Cache and use it for all request.
        //Cache expiry can be set based on authResult.ExpiresOn.
        //Expire time depend on server settings.
        //Get Token for API Call
        public async Task<AuthenticationResult> Authenticate()
        {
            try
            {
                return await authContext.AcquireTokenAsync(Constant.AzureAd.ClientId, cred);
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                return null;
            }
        }


        //Get API, Get Data from API using Access Token and Path
        public async Task<List<T>> GetRequestAsync<T>(string path)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var authResult = await Authenticate();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authResult.AccessToken);
                    client.BaseAddress = new Uri(Constant.ApiBaseUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage response = await client.GetAsync(path);
                    if (response.IsSuccessStatusCode)
                    {
                        var data = await response.Content.ReadAsStringAsync();
                        return JsonConvert.DeserializeObject<List<T>>(data);
                    }
                    else
                    {
                        Logger.Log("Internal server Error");
                        return default(List<T>);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                return default(List<T>);
            }
        }

        //Post Data to API to save in DB
        public async Task<TReturn> PostDataAsync<TParam, TReturn>(string path, TParam data)
        {

            try
            {
                using (var client = new HttpClient())
                {
                    var authResult = await Authenticate();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authResult.AccessToken);
                    client.BaseAddress = new Uri(Constant.ApiBaseUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var content = new StringContent(JsonConvert.SerializeObject(data), System.Text.Encoding.Default, "application/json");
                    HttpResponseMessage response = await client.PostAsync(path, content);
                    if (response.IsSuccessStatusCode)
                    {
                        var returnData = await response.Content.ReadAsStringAsync();
                        return JsonConvert.DeserializeObject<TReturn>(returnData);

                    }
                    else
                    {
                        Logger.Log("Internal server Error");
                        return default;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                return default;
            }
        }
    }
}
