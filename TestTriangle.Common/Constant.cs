﻿using Microsoft.AspNetCore.Authentication.AzureAD.UI;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestTriangle.Common
{
    public class Constant
    {
        public static AzureADOptions AzureAd { get; set; }

        public static string Authority { get; set; }

        public static Dictionary<string, string> UrlConst { get; set; }

        public static string ApiBaseUrl { get { return UrlConst["ApiBaseUrl"]; } }
        public static string UserUrl { get { return UrlConst["UserUrl"]; } }

    }
}
