﻿using Microsoft.AspNetCore.Authentication.AzureAD.UI;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestTriangle.Common
{
    //We may change const and set it for multi language.
    public class ConstMsg
    {
        #region Fail Message
        public const string FailAtServer = "Error on server, please contact admin";
        public const string FailAtAPI = "Fail to comunicate with API, please contact admin";
        public const string FailInvalidName = "Please set user name";
        #endregion Fail Message

        #region Sucess Message
        public const string SucessInsert = "User information inserted with id {0}";
        #endregion Sucess Message

    }
}
