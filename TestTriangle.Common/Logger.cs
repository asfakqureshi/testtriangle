﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestTriangle.Common
{
    public class Logger
    {
        /// <summary>
        /// Log
        /// </summary>
        /// <param name="ex"></param>
        public static void Log(Exception ex)
        {
            //Not in scope so set output in Console
            Console.WriteLine(string.Format("Error : {0}", ex.Message));

        }

        /// <summary>
        /// Log Test
        /// </summary>
        /// <param name="info"></param>
        public static void Log(string info)
        {
            //Not in scope so set output in Console
            Console.WriteLine(string.Format("Info : {0}", info));

        }
    }
}
