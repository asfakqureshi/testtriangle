﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TestTriangle.Common;
using TestTriangle.Model;
using TestTriangle.Web.Models;

namespace TestTriangle.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        IHttpHelper httpHelper;
        public HomeController(IHttpHelper httpHelper)
        {
            this.httpHelper = httpHelper;

        }

        //Instead of this we can send token to UI and perform Get and Post from UI using Ajax.
        //It's all depend on requirment, let me know if you want to check that.
        public async Task<IActionResult> Index()
        {
            if (httpHelper == null)
            {
                ViewBag.ErrorMessage = ConstMsg.FailAtServer;
                return View();
            }
            var users = await httpHelper.GetRequestAsync<Users>(Constant.UserUrl);
            if(users == null)
            {
                ViewBag.ErrorMessage = ConstMsg.FailAtAPI;
                return View();
            }
            return View(users);
        }

        //I can perform insert data and retrive it in once but all is depend on requirement
        [HttpPost]
        public async Task<IActionResult> Index(Users user)
        {
            if (httpHelper == null || user == null)
            {
                ViewBag.ErrorMessage = ConstMsg.FailAtServer;
                return null;
            }

            var userId = await httpHelper.PostDataAsync<Users, int>(Constant.UserUrl, user);
            SetMessage(userId);
            var users = await httpHelper.GetRequestAsync<Users>(Constant.UserUrl);
            return View(users);

        }

        private void SetMessage(int userId)
        {
            if (userId == default)
            {
                ViewBag.ErrorMessage = ConstMsg.FailAtAPI;
            }
            else if (userId == -1)
            {
                ViewBag.ErrorMessage = ConstMsg.FailInvalidName;
            }
            else if (userId > 0)
            {
                ViewBag.SuccessMessage = string.Format(ConstMsg.SucessInsert, userId);
            }
        }


        [AllowAnonymous]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
