# TestTriangle

Created for Test - TestTriangle

# Cover Below
Please follow the points below, while creating the applications. Please DO NOT include any
PII and any sample data, should be dummy only.
1) Create a MVC app and a Web API. Both should be developed using .NET Core.
2) Create a single page to send/receive request/response to/from the API.
3) Develop at least one operation in API (Could be anything of your choice, GET/ POST,
POST being preferable).
4) Both web app and API should use OIDC/OAuth2 authentication. May use Azure
Active Directory or any other identity providers (e.g. Google, Facebook etc.)
5) Use any business entity of your choice.
6) Use Entity Framework/ Dapper as ORM. You can use some in-memory data instead
of a persistence store.
7) Should have unit test cases for all the methods written and should use concept of
mocking wherever applicable.
8) Please use proper comment during each commit.
9) Web UI should be mobile responsive.
10) Quality of code would be observed.
11) Use SOLID principles.
12) Please use necessary design patterns (please mention the reason to choose that
one) and best practices while developing code.
13) Please share the whole code via Github/ Bitbucket.

# Node
I added few of unit test method, to show that I worked with TDD.
It's very few case, might be not sufficient in actual case, but it's show only that I can write in case of complex business use case.
In case you need more unit test then I can write it.
I can use many other fundamentals or ways, but due to only 1 page and 1 object post and get I did this only.

Let me know in case any issue
asfakqureshi@gmail.com
