﻿using Moq;
using System.Collections.Generic;
using TestTriangle.DBProvider.Interface;
using TestTriangle.Model;

namespace TestTriangle.API.Test
{
    public class ApiBaseTest
    {
        protected const string DUMMY = "dummy";
        protected Mock<IUserRepository> mockUserRepo = null;
        
        public ApiBaseTest()
        {
            SetupMocks();
        }

        private void SetupMocks()
        {
            mockUserRepo = new Mock<IUserRepository>();
            var user = new Users { Name = DUMMY };
            var mockList = new List<Users>() { user  } as IEnumerable<Users>;
            mockUserRepo.Setup(x => x.Get()).Returns(mockList);
            mockUserRepo.Setup(x => x.Insert(It.IsAny<Users>())).Returns(  1 );
            mockUserRepo.Setup(x => x.Insert(It.Is<Users>(s=> string.IsNullOrWhiteSpace(s.Name) ))).Returns( -1);

        }

    }
}
