﻿using System.Linq;
using TestTriangle.API.Controllers;
using TestTriangle.Model;
using Xunit;

namespace TestTriangle.API.Test
{
    public class UsersControllerTest : ApiBaseTest
    {
        private UsersController controller = null;
        public UsersControllerTest()
        {
            controller = new UsersController(mockUserRepo.Object);
        }

        [Fact]
        public void GetALL_Data()
        {
            var response = controller.Get();
            Assert.True(response != null);
            Assert.True(1 == response.Value.ToList().Count);
        }

        [Fact]
        public void InsertData_ValidData()
        {
            var response = controller.Post(new Users { Name = DUMMY, Email = DUMMY, Phone = DUMMY });
            Assert.True(response != null);
            Assert.True(1 == response.Value);

        }

        [Fact]
        public void InsertData_NoName_FailInsert()
        {
            var response = controller.Post(new Users { Email = DUMMY, Phone = DUMMY });
            Assert.True(response != null);
            Assert.True(-1 == response.Value);

        }

        [Fact]
        public void InsertData_NULL_FailInsert()
        {
            var response = controller.Post(null);
            Assert.True(response != null);
            Assert.True(0 == response.Value);

        }
    }
}
